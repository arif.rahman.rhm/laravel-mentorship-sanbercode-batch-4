<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $strObject = new Str;
        \App\Models\User::create([
            'name'	=> $strObject->random(20),
            'email'	=> $strObject->random(10) . '@adminsiapa.com',
            'password'	=> bcrypt('admin123456'),
            'id_role' => 1

    ]);
    }
}

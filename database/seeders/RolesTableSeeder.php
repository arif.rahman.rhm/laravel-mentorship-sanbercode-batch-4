<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('roles')->count() == 0){

            DB::table('roles')->insert([

                [
                    'nama_role' => 'admin',
                ],
                [
                    'nama_role' => 'mahasiswa',
                ]

            ]);
    }
}
}